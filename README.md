# Telegram Bot demo  using the .NET Client for Telegram Bot API

A DotNet Core 3.1 telegram bot console app

This a simple implementation, with little complexity just show casing how to create and code up a telegram bot

* Create a Telegram Bot using the BotFather

``` text
Go to your bot settings and enable the Inline Mode
```

* Containerize the app into a docker container with docker-compose  support

```text
docker-compose can be confusing but it is a lot more elegant a simple. Here I create the `hello world` of docker-compose.yml to host and run little awesome bot
```

## Running the app

``` bash
dotnet MyTeleBot.dll # run straight from the terminal

# if you want to run the container
Navigate to ./MyTeleBot
docker-compose up -d # run detached
docker-compose down # stop 
```

## Building the app

dotnet build MyTeleBot.sln

## Docker Compose quick start guide

Navigate to your working directory. Below are the most commands commands when working with docker-compose

* `docker-compose exec mytelebot-service bash # Here we login to a running mytelebot-service container`
* `docker-compose run mytelebot-service # Here we will spin up another container`
* `docker-compose logs # Here we view logs`
* `docker-compose logs -f # Here we tail the logs`
* `docker-compose stop # Here we stop the running containers/services listed in the docker-compose.yml file`
* `docker-compose start # Here we start the stopped containers/services listed in the docker-compose.yml file`
* `docker-compose restart # Stop/Start containers usually after making code changes hosted in mounted volumes`
* `docker-compose kill # Here we terminate containers/services aggressively listed in the docker-compose.yml file`
* `docker-compose build # Here build the container using cache new changes will be appended`
* `docker-compose build --no-cache # Here build the container NO CACHE, will rebuild everything from scratch`
* `docker-compose up -d # Will build apply any changes and startup the container`

## Docker quick start guide

``` bash
#! /bin/bash

docker build -t mytelebot . # build your container
docker run -d --name mytelebot mytelebot # run your container detached
docker stop mytelebot # stop it

```

---

## References
  
[.NET Client for Telegram Bot API](https://github.com/TelegramBots/telegram.bot)  
[Telegram Bot API](https://core.telegram.org/bots/api)  
[Docker Compose](https://docs.docker.com/compose/install/#install-compose)  
[Dockerize an ASP.NET Core application](https://docs.docker.com/engine/examples/dotnetcore/)  
