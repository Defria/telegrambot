﻿namespace MyTeleBot.Enums
{
    public enum InlineOption
    {
        Help = 1,
        Info,
        About,
        More
    }
}