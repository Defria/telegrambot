﻿using System.Threading.Tasks;
using MyTeleBot.Services;

namespace MyTeleBot
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var idManService = new IdManService();
            var telegramService = new TelegramService(idManService);
            await telegramService.Run();
        }
    }
}