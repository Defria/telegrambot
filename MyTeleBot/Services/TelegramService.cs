﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using MyTeleBot.Enums;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyTeleBot.Services
{
    public class TelegramService
    {
        private readonly IdManService _idManService;
        private readonly TelegramBotClient _bot;

        public TelegramService(IdManService idManService)
        {
            _idManService = idManService;
            const string botToken = "1691881830:AAHyqTAgSTvSbpveJttzEXXLVXJ_KwQrq8E";
            _bot = new TelegramBotClient(botToken);
        }

        public async Task Run()
        {
            var me = await _bot.GetMeAsync();
            var cts = new CancellationTokenSource();
            _bot.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync), cts.Token);
            Console.WriteLine($"Listening {me.Username}");
            if (!Debugger.IsAttached)
                await KeepAlive(cts);
            Console.ReadLine();
            cts.Cancel();
        }
        

        private async Task KeepAlive(CancellationTokenSource cts)
        {
            await Task.Delay(Timeout.Infinite, cts.Token).ConfigureAwait(false);
        }

        private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
            CancellationToken cancellationToken)
        {
            Console.WriteLine($"Processing update {update.Type}");
            var handler = update.Type switch
            {
                UpdateType.Message => BotOnMessageReceived(update.Message),
                UpdateType.EditedMessage => BotOnMessageReceived(update.Message),
                UpdateType.CallbackQuery => BotOnCallbackQueryReceived(update.CallbackQuery),
                UpdateType.InlineQuery => BotOnInlineQueryReceived(update.InlineQuery),
                UpdateType.ChosenInlineResult => BotOnChosenInlineResultReceived(update.ChosenInlineResult),
                _ => UnknownUpdateHandlerAsync(update)
            };

            try
            {
                await handler;
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(botClient, exception, cancellationToken);
            }
        }

        private async Task UnknownUpdateHandlerAsync(Update update)
        {
            var text = $"Method not implemented {update.Type}";
            await _bot.SendTextMessageAsync(update.Message.MessageId, text);
        }

        private async Task BotOnChosenInlineResultReceived(ChosenInlineResult updateChosenInlineResult)
        {
            await Task.Run(() => Console.WriteLine("Method not implemented"));
        }

        private async Task BotOnInlineQueryReceived(InlineQuery updateInlineQuery)
        {
            await Task.Run(() => Console.WriteLine("Method not implemented"));
        }

        private async Task BotOnCallbackQueryReceived(CallbackQuery result)
        {
            Enum.TryParse(result.Data, out InlineOption inlineOption);
            var option = inlineOption switch
            {
                InlineOption.About => SendAboutMessage(result.Message),
                InlineOption.Help => SendHelpMessage(result.Message),
                InlineOption.More => SendUsage(result.Message),
                _ => HandleUnknownOptions()
            };

            await option;
        }

        private async Task SendHelpMessage(Message message)
        {
            var textMessage = @"
Checkout this cool website to get started
Copy one of the FAKE id number and paste it here
See what I can do
[Generate Fake South African ID Numbers](https://chris927.github.io/generate-sa-idnumbers/#)  
";
            await _bot.SendTextMessageAsync(message.Chat.Id, textMessage, ParseMode.MarkdownV2);
        }

        private async Task HandleUnknownOptions()
        {
            await Task.Run(() => Console.Write("Method not implemented"));
        }

        private async Task SendAboutMessage(Message message)
        {
            var text =
                "About page: \nI'm here to help you validate an RSA ID number.\nJust type the ID number below and I will validate for you.";
            await _bot.SendTextMessageAsync(message.Chat.Id, text);
        }

        private async Task BotOnMessageReceived(Message message)
        {
            Console.WriteLine($"Receive message type: {message.Type}");
            if (message.Type != MessageType.Text)
                return;

            var textInfo = new CultureInfo("en-US", false).TextInfo;
            if (BigInteger.TryParse(message.Text, out _))
            {
                await ValidateIdNumber(message);
                await _bot.SendTextMessageAsync("-552805208",
                    $"{textInfo.ToTitleCase(message.Chat.Username)} Just validated ID {message.Text}");
                return;
            }

            var action = message.Text.Split(' ').First() switch
            {
                "/help" => SendHelpMessage(message),
                "/info" => SendInlineKeyboard(message),
                "/about" => SendAboutMessage(message),
                _ => SendInlineKeyboard(message)
            };
            await action;
        }

        private async Task ValidateIdNumber(Message message)
        {
            var specialCharacters = @"\r\n";
            var result = _idManService.ValidateIdNumber(message.Text);
            var identity = _idManService.GetIdentity(message.Text);
            var textMessage = $@"```{result.Replace(specialCharacters, " ")}```";
            if (identity != null)
                textMessage = $@"
```{result.Replace(specialCharacters, " ")}```
*Validation Results: *  
IdNumber: {identity.IdNumber}  
Date Of Birth: {DateTime.ParseExact(identity.DateOfBirth, "yyMMdd", null).ToLongDateString()}  
Gender: {identity.Gender}  
Citizen: {identity.Citizenship}  
[Generate Fake South African ID Numbers](https://chris927.github.io/generate-sa-idnumbers/#)  
";

            await _bot.SendTextMessageAsync(message.Chat.Id, textMessage, ParseMode.MarkdownV2);
        }

        private async Task SendInlineKeyboard(Message message)
        {
            await _bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
                // first row
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("Help", "Help"),
                    InlineKeyboardButton.WithCallbackData("Info", "Info")
                },
                // second row
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("About", "About"),
                    InlineKeyboardButton.WithCallbackData("More", "More")
                }
            });

            await _bot.SendTextMessageAsync(
                message.Chat.Id,
                "What would you like to do?",
                replyMarkup: inlineKeyboard
            );
        }

        private async Task SendReplyKeyboard(Message message)
        {
            var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                new[]
                {
                    new KeyboardButton[] {"1.1", "1.2"},
                    new KeyboardButton[] {"2.1", "2.2"}
                },
                true
            );

            await _bot.SendTextMessageAsync(
                message.Chat.Id,
                "Choose",
                replyMarkup: replyKeyboardMarkup
            );
        }

        private async Task SendUsage(Message message)
        {
            const string usage = "Here are some options to get you started:\n" +
                                 "/help   - Get help\n" +
                                 "/info - Tell you what I can do\n" +
                                 "/about   - Tell you about me\n";
            await _bot.SendTextMessageAsync(
                message.Chat.Id,
                usage,
                replyMarkup: new ReplyKeyboardRemove()
            );
        }

        private async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception,
            CancellationToken cancellationToken)
        {
            var errorMessage = exception switch
            {
                ApiRequestException apiRequestException =>
                    $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(errorMessage);
        }
    }
}