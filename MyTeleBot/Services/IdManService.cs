﻿using System.Collections.Generic;
using System.Linq;
using MyTeleBot.Models;
using RestSharp;
using RestSharp.Authenticators;

namespace MyTeleBot.Services
{
    public class IdManService
    {
        private const string BaseUrl = "http://api.defria.com:5000/api/";
        private readonly RestClient _client;

        public IdManService()
        {
            _client = new RestClient(BaseUrl);
        }

        public Auth Login()
        {
            const string resourcePath = "auth/login";

            var user = new User
            {
                Username = "john",
                Password = "password"
            };
            var request = new RestRequest {Resource = resourcePath};
            request.AddJsonBody(user);

            return _client.PostAsync<Auth>(request).Result;
        }

        public string ValidateIdNumber(string idnumber)
        {
            var resourcePath = $"identities/{idnumber}";
            var request = new RestRequest {Resource = resourcePath};
            var token = Login().Token;
            _client.Authenticator = new JwtAuthenticator(token);
            return _client.Put(request).Content;
        }

        public Identity GetIdentity(string idNumber)
        {
            var resourcePath = "identities";
            var request = new RestRequest {Resource = resourcePath};
            var token = Login().Token;
            _client.Authenticator = new JwtAuthenticator(token);
            var identities = _client.GetAsync<List<Identity>>(request);
            var identity = identities.Result.FirstOrDefault(i => string.Equals(i.IdNumber, idNumber));
            return identity;
        }
    }
}