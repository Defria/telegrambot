﻿using Newtonsoft.Json;

namespace MyTeleBot.Models
{
    public class Identity
    {
        [JsonProperty("idNumber")] public string IdNumber { get; set; }
        [JsonProperty("dateOfBirth")] public string DateOfBirth { get; set; }
        [JsonProperty("gender")] public string Gender { get; set; }
        [JsonProperty("citizenship")] public string Citizenship { get; set; }
    }
}