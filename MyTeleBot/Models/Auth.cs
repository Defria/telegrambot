﻿using Newtonsoft.Json;

namespace MyTeleBot.Models
{
    public class Auth
    {
        [JsonProperty("token")] public string Token { get; set; }
    }
}