﻿using System.ComponentModel;
using MyTeleBot.Services;
using Xunit;

namespace IDValidatorTests.Services
{
    public class IdManServiceTests
    {
        [Fact]
        [Category("Integration")]
        public void Login_WhenCalled_ShouldReturnToken()
        {
            // Arrange
            var service = new IdManService();

            // Act
            var token = service.Login().Token;

            // Assert
            Assert.False(string.IsNullOrWhiteSpace(token));
        }


        [Fact]
        [Category("Integration")]
        public void Validate()
        {
            // Arrange
            var service = new IdManService();

            // Act
            var response = service.ValidateIdNumber("8801234800186");

            // Assert
            Assert.True(response.Contains("success"));
        }
    }
}